<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\TestsImport;
use DB;



class FileController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function getForm()
    {
        return view('upload-form');
    }

    public function upload(Request $request)
    {
    	//$tmp = 0;
        foreach ($request->file() as $file) {
            foreach ($file as $f) {
            	$filename = time().'_'.$f->getClientOriginalName();
                $f->move(storage_path('exel_files'), $filename);
                DB::table('userfile')->insert(['filename' => $filename, 'user'  => $request->user]);
                //$tmp = $filename;
            }
        }
        //return view ('test', ['file' => $tmp]);
        return redirect('/');
    }

	public function getFiles()
	{

	    $f = Storage::disk('exel_files');
	    $userfiles = DB::table('userfile')->get();
	    
	    $array = array();
	    $files = $f->allFiles();
	    $filenames = array();
	    $count = 0;
	    foreach ($files as $file) {
	    	foreach ($userfiles as $uf) {
	    		if ($file == $uf->filename and $uf->user == Auth::user()->id) {
	    			array_push($array, Excel::toArray(new TestsImport, storage_path('exel_files/'.$file))[0]);
	    			array_push($filenames, $file);
	    			if ($count >= 4) {
	    				break;
	    			}
	    			$count++;
	    		}
	    	}
	    	if ($count >= 4) {
	    		break;
	    	}
	    }

	    
	    foreach ($files as $file) {
	    	// array_push($array, Excel::toArray(new TestsImport, storage_path('exel_files/'.$file))[0]);
	    }
	    //$array = Excel::toArray(new TestsImport, storage_path('exel_files/'.$files[0]))[0];
	    
	    
	    return view('dbt',['array' => $array, 'files' => $files, 'userfile' => $userfiles, 'filenames' => $filenames]);
	}

	public function delete(Request $request)
	{
	    $f = Storage::disk('exel_files');
	    $f->delete($request->filename);
	    DB::table('userfile')->where('filename', '=', $request->filename)->delete();
	    return redirect('/');
	}

	public function show(Request $request)
	{
	    $f = Storage::disk('exel_files');
	    
	    $array = Excel::toArray(new TestsImport, storage_path('exel_files/'.$request->filename))[0];
	    
	    return view('show',['array' => $array, 'file' => $request->filename]);
	}
}
