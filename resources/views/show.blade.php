@extends('layouts.app')

@section('content')
	<script src="https://code.highcharts.com/highcharts.js"></script>

<script type="text/javascript">
    $(function () { 
    	<?php $col = 1; ?>
		<?php foreach ($array as $a) {
			if (array_column($array, $col) == null) {
				break;
			}
			echo "var data".$col." = ".json_encode(array_column($array, $col)).";\n";
			echo "data".$col.".shift();\n";
			$col ++;
			
		} 
		?>
        var data_x = <?php echo json_encode(array_column($array, 0)); ?>;
        data_x.shift();
   	 $('#container').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: '{{$file}}'
        },
        xAxis: {
        	title: {
        		text: <?php echo "'".$array[0][0]."'"; ?>
        	},
            categories: data_x
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        series: [
			<?php $col = 1; ?>
			<?php foreach ($array as $a) {
				if (array_column($array, $col) == null) {
					break;
				}
				echo "{ data: data".$col.",\n";
				echo " name: '".$array[0][$col]."'},\n";


				$col ++;	
			} ?>
	    ]    
	});
});
</script>
<div class="container-fluid h-100 mb-2 mt-1 border border-secondary rounded">
    <a class="btn btn-primary mt-1" href="{{route('upload_form')}}"> Назад </a>
    <div class="row h-100">
        <div class="col-md-12 h-100">
            <div class="panel panel-default h-100">
                <div class="panel-heading h-100"></div>
                <div class="panel-body h-100">
                    <div id="container" class="h-100"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
