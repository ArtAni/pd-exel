<!DOCTYPE html>
<html>
<head>
    <title>HighChart</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
</head>
<body>


<script type="text/javascript">


$(function () { 


    var data_click = <?php echo json_encode(array_column($array[0], 1)); ?>;

    var data_viewer = <?php echo json_encode(array_column($array[0], 2)); ?>;
    var x_axis = <?php echo json_encode(array_column($array[0], 0)); ?>;

    data_click.shift();
    data_viewer.shift();
    x_axis.shift();

    $('#container').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: x_axis
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        series: [{
            name: 'data_1',
            data: data_click
        }, {
            name: 'data_2',
            data: data_viewer
        }]
    });
});


</script>


<div class="container">
    <br/>
    <h2 class="text-center"></h2>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <div id="container"></div>
                </div>
            </div>
        </div>
    </div>
</div>




</body>
</html>






