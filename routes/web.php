<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/',['as' => 'upload_form', 'uses' => 'FileController@getFiles']);
Route::post('/',['as' => 'upload_file','uses' => 'FileController@upload']);
Route::get('/all',['as' => 'upload_all', 'uses' => 'FileController@getFiles']);
Route::get('/delete/{filename}',['as' => 'upload_delete','uses' => 'FileController@delete']);
Route::get('/show/{filename}', ['as' => 'show_file', 'uses' => 'FileController@show']);

Auth::routes();
Route::get('/home',['as' => 'upload_form', 'uses' => 'FileController@getFiles']);
